//
//  DataBlob.swift
//  Decibel Atlas
//
//  Created by CES_the_reaper on 4/10/17.
//  Copyright © 2017 CES_the_reaper. All rights reserved.
//

import Foundation
public class DataBlob{
    var decibels:Double,latitude:Double,longitude:Double,altitude:Double,accuracy:Double,time:Double
    init(decibels:Double,latitude:Double,longitude:Double,altitude:Double,accuracy:Double,time:Double){
        self.decibels = decibels
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.accuracy = accuracy
        self.time = time
    }
}
