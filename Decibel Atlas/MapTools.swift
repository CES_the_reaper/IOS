//
//  MapTools.swift
//  Decibel Atlas
//
//  Created by CES_the_reaper on 4/10/17.
//  Copyright © 2017 CES_the_reaper. All rights reserved.
//

import Foundation
import MapKit
import UIKit
class MapTools: NSObject, MKMapViewDelegate{
    
    //let regionRadius: CLLocationDistance = 1000

    
    
    override init(){
    }
    static var circleColor:UIColor = UIColor.blue
    static func makeCircle(latitude: Double, longitude: Double, radius: Double)->MKCircle{
        let at = CLLocationCoordinate2D.init(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        let distance = CLLocationDistance(radius)
        let circle: MKCircle = MKCircle.init(center:at,radius:distance)
        
        return circle
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let overlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: overlay)
            circleRenderer.fillColor = MapTools.circleColor
            //circleRenderer.strokeColor = UIColor.green
            return circleRenderer
        }
        print("Failure in MapOverlayRenderer")
        return MKOverlayRenderer()
    }

    func centerMapOnLocation(location: CLLocation, map: MKMapView, radius regionRadius: CLLocationDistance) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        map.setRegion(coordinateRegion, animated: true)
    }

    
}
